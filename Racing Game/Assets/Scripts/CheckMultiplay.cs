﻿using UnityEngine;
using System.Collections;

public class CheckMultiplay : MonoBehaviour 
{
	GameObject settingObject;
	GameObject currentUI;
	StartOptions start;
	SetChoice setting;

	void Start () 
	{
		settingObject = GameObject.Find("SettingCapture");
		setting = settingObject.GetComponent<SetChoice>();
		currentUI = GameObject.Find("UI");
		start = currentUI.GetComponent<StartOptions>();
		if (setting.getMultiPlayChoice())
		{
			start.sceneToStart = 4;
		}
		else
		{
			start.sceneToStart = 5;
		}
	}
	
	void Update () 
	{
	
	}
}
