﻿using UnityEngine;
using System.Collections;

public class TrackSelection : MonoBehaviour 
{
	public int selection = 0;
	private SetChoice setting;

	void Start () 
	{
		GameObject settingObject = GameObject.Find("SettingCapture");
		setting = settingObject.GetComponent<SetChoice>();
	}

	public void SendSelection()
	{
		setting.setMapChoice(selection);
	}
}
