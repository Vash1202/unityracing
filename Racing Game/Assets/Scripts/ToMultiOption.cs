﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ToMultiOption : MonoBehaviour 
{
	public int sceneToStart = 1;
	
	public void NextMenu()
	{
		SceneManager.LoadScene (sceneToStart);
	}
}
