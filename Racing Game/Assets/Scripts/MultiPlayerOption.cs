﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MultiPlayerOption : MonoBehaviour 
{
	private SetChoice setting;

	void Start () 
	{
		GameObject settingObject = GameObject.Find("SettingCapture");
		setting = settingObject.GetComponent<SetChoice>();
	}

	public void turnOnMultiplay()
	{
		setting.setMultiplay();
	}
}
