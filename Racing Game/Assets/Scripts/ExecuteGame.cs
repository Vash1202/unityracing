﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ExecuteGame : MonoBehaviour
{
	public void Start () 
	{
		GameObject settingObject = GameObject.Find("SettingCapture");
		GameObject cameraMaster = GameObject.Find("Main Camera");
		SetChoice setting = settingObject.GetComponent<SetChoice>();
		GameObject car = (GameObject) Instantiate(setting.getCarChoice());
		GameObject map = (GameObject) Instantiate(setting.getMapChoice());
		GameObject network = GameObject.Find("Network Manager");
		SingleCameraFollow camera = cameraMaster.GetComponent<SingleCameraFollow>();
		camera.setTarget(car.transform);
		camera.xOffset = 0;
		Instantiate(cameraMaster);
		if (!setting.getMultiPlayChoice())
		{
			Destroy(network);
		}
	}
}
