﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SetChoice : MonoBehaviour 
{
	public GameObject carChoice;
	public GameObject trackChoice;
	public GameObject[] cars;
	public GameObject[] maps;
	private bool multiplay = false;

	public void setCarChoice(int selection)
	{
		carChoice = cars[selection];
	}

	public GameObject getCarChoice()
	{
		return carChoice;
	}

	public void setMapChoice(int selection)
	{
		trackChoice = maps[selection];
	}

	public GameObject getMapChoice()
	{
		return trackChoice;
	}

	public void setMultiplay()
	{
		multiplay = true;
	}

	public bool getMultiPlayChoice()
	{
		return multiplay;
	}
}
