﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuTransition : MonoBehaviour 
{
	public int sceneToStart = 1;
	
	public void NextMenu()
	{
		SceneManager.LoadScene (sceneToStart);
	}
}
