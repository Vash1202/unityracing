# README #

To play, clone repo and run executable file in RaceForTime folder. To run on Android, transfer RaceForTime.apk application into your Android phone.

### What is this repository for? ###

So you can play with the code as you like.

### How do I get set up? ###

Make sure you have atleast Unity 3.5 if you want to edit the game Assets and add your own twist.

### Contribution guidelines ###

Contributions made by:

Hank Lin,
Lucas Seibert,
Dean Steuer,
Elijah Woolford,
Ang Li,
Austin Schaffer